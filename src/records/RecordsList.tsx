import React, { FC, ChangeEvent, useState, useEffect } from "react";
import styled from "styled-components";
import { lighten } from "polished";

import { IRecord, INextPage, ICondition } from "../types/records";
import Button from "../components/Button";
import { conditions } from "../constants/records";

const StyledRecordsList = styled.div`
  background-color: ${({ theme }) => theme.colors.navy};
  ul {
    list-style-type: none;
    text-align: left;
    padding: 0;
    margin: 0;
    li {
      padding: 0.75rem 1rem;
      display: grid;
      align-items: center;
      grid-template-columns: 1fr;
      grid-gap: 1rem;
      border-bottom: 1px solid ${({ theme }) => lighten(0.1, theme.colors.navy)};
      @media screen and (min-width: 900px) {
        grid-template-columns: 2fr 2fr 1fr 1fr auto;
      }
    }
  }

  .search-input {
    width: 100%;
    font-size: 1.25rem;
    box-sizing: border-box;
    padding: 1rem;
  }

  .load-more-container,
  .loader-container {
    padding: 1rem 0;
  }

  .loader {
    border: 0.5rem solid ${({ theme }) => theme.colors.grey};
    border-top: 0.5rem solid ${({ theme }) => theme.colors.blue};
    border-radius: 50%;
    width: 2rem;
    height: 2rem;
    animation: spin 2s linear infinite;
    margin: 0 auto;
  }

  @keyframes spin {
    0% {
      transform: rotate(0deg);
    }
    100% {
      transform: rotate(360deg);
    }
  }
`;

interface IProps {
  records: IRecord[];
  nextPage: INextPage;
  activateRecordForm: (id: number) => void;
  fetchRecords: (request: RequestInfo) => void;
  dataLoading: boolean;
}

const RecordsList: FC<IProps> = (props) => {
  const [searchQuery, setSearchQuery] = useState("");
  const [filteredRecords, setFilteredRecords] = useState<IRecord[]>(
    props.records
  );

  const handleSearchQueryChange = (e: ChangeEvent<HTMLInputElement>) => {
    setSearchQuery(e.target.value);
  };

  const conditionName = (recordCondition: ICondition) =>
    conditions.filter((condition) => condition.key === recordCondition)[0]
      ?.label;

  useEffect(() => {
    const searchIncludes = (attribute: string) =>
      attribute.toLowerCase().includes(searchQuery.toLowerCase());
    // Debounce the searchQuery if value exists
    if (searchQuery) {
      const timer = setTimeout(() => {
        const recordsFilter = props.records.filter(
          (record) =>
            searchIncludes(record.album_title) ||
            searchIncludes(record.artist.name)
        );

        setFilteredRecords(recordsFilter);
      }, 250);
      return () => clearTimeout(timer);
    } else {
      setFilteredRecords(props.records);
    }
  }, [searchQuery, props.records]);

  return (
    <StyledRecordsList data-testid="records-list">
      <input
        className="search-input"
        value={searchQuery}
        onChange={handleSearchQueryChange}
        placeholder="Search for a record..."
      />
      {filteredRecords.length === 0 && !props.dataLoading ? (
        <p>No results match your search :(</p>
      ) : (
        <ul>
          {filteredRecords.map((record: IRecord) => (
            <li key={`record-${record.id}`}>
              <span>{record.album_title}</span>
              <span>{record.artist.name}</span>
              <span>{conditionName(record.condition)}</span>
              <span>{record.year}</span>
              <div>
                <Button onClick={() => props.activateRecordForm(record.id)}>
                  Edit
                </Button>
              </div>
            </li>
          ))}
        </ul>
      )}
      {props.dataLoading && (
        <div className="loader-container">
          <div className="loader" />
        </div>
      )}
      {props.nextPage && (
        <div className="load-more-container">
          <Button
            disabled={props.dataLoading}
            onClick={() => props.fetchRecords(props.nextPage as RequestInfo)}
          >
            Load More
          </Button>
        </div>
      )}
    </StyledRecordsList>
  );
};

export default RecordsList;
