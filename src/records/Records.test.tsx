import React from "react";
import { waitFor } from "@testing-library/react";
import Records from "./Records";

import ReactModal from "react-modal";
import { renderWithProviders } from "../testUtils";
ReactModal.setAppElement("*");

describe("Records", () => {
  beforeEach(() => {
    fetchMock.resetMocks();
  });
  test("fetches and displays fetched record", async () => {
    fetchMock.mockResponseOnce(
      JSON.stringify({
        results: [
          {
            album_title: "Knausgaard Tacos Mumblecore",
            year: 1991,
            condition: "good",
            artist: { name: "The Who", id: 5 },
          },
        ],
        nextPage: null,
      })
    );
    const { getByText } = renderWithProviders(<Records />);

    await waitFor(() => {
      expect(getByText("Knausgaard Tacos Mumblecore")).toBeInTheDocument();
    });
  });
});
