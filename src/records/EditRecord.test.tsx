import React from "react";
import { fireEvent, waitFor } from "@testing-library/react";
import ReactModal from "react-modal";
ReactModal.setAppElement("*");

import EditRecord from "./EditRecord";
import { IRecord } from "../types/records";
import { renderWithProviders } from "../testUtils";

const artists = [
  { id: 1, name: "Test Artist Name" },
  { id: 2, name: "Test Artist Name 2" },
];

const record: IRecord = {
  album_title: "Test Album Title",
  artist: artists[0],
  condition: "good",
  year: 2000,
  id: 0,
};

describe("EditRecord", () => {
  test("should render the component", () => {
    const { getByText } = renderWithProviders(
      <EditRecord
        onCloseForm={jest.fn()}
        artists={artists}
        record={record}
        updateArtist={jest.fn()}
        updateRecord={jest.fn()}
      />
    );

    expect(getByText("Edit Record")).toBeInTheDocument();
  });

  test("should fire update record when update record is clicked", async () => {
    const updateRecord = jest.fn();
    const { getByText } = renderWithProviders(
      <EditRecord
        onCloseForm={jest.fn()}
        artists={artists}
        record={record}
        updateArtist={jest.fn()}
        updateRecord={updateRecord}
      />
    );

    const updateRecordButton = getByText("Update Record");

    fireEvent.click(updateRecordButton);
    await waitFor(() => {
      expect(updateRecord).toBeCalledTimes(1);
      expect(updateRecord).toBeCalledWith(record);
    });
  });

  test("should update record artist correctly", async () => {
    const updateRecord = jest.fn();
    const { getByText, getByLabelText } = renderWithProviders(
      <EditRecord
        onCloseForm={jest.fn()}
        artists={artists}
        record={record}
        updateArtist={jest.fn()}
        updateRecord={updateRecord}
      />
    );

    const updateRecordButton = getByText("Update Record");
    const recordArtistSelect = getByLabelText("Record Artist");

    fireEvent.change(recordArtistSelect, { target: { value: "2" } });

    fireEvent.click(updateRecordButton);

    await waitFor(() => {
      expect(updateRecord).toBeCalledTimes(1);
      expect(updateRecord).toBeCalledWith({ ...record, artist: artists[1] });
    });
  });

  test("should update record album_title correctly", async () => {
    const updateRecord = jest.fn();
    const { getByText, getByLabelText } = renderWithProviders(
      <EditRecord
        onCloseForm={jest.fn()}
        artists={artists}
        record={record}
        updateArtist={jest.fn()}
        updateRecord={updateRecord}
      />
    );

    const updateRecordButton = getByText("Update Record");
    const albumTitleInput = getByLabelText("Album Title");

    fireEvent.change(albumTitleInput, {
      target: { value: "A New Test Title" },
    });

    fireEvent.click(updateRecordButton);

    await waitFor(() => {
      expect(updateRecord).toBeCalledTimes(1);
      expect(updateRecord).toBeCalledWith({
        ...record,
        album_title: "A New Test Title",
      });
    });
  });

  test("should update record condition correctly", async () => {
    const updateRecord = jest.fn();
    const { getByText, getByLabelText } = renderWithProviders(
      <EditRecord
        onCloseForm={jest.fn()}
        artists={artists}
        record={record}
        updateArtist={jest.fn()}
        updateRecord={updateRecord}
      />
    );

    const updateRecordButton = getByText("Update Record");
    const conditionSelect = getByLabelText("Condition");

    fireEvent.change(conditionSelect, {
      target: { value: "fair" },
    });

    fireEvent.click(updateRecordButton);

    await waitFor(() => {
      expect(updateRecord).toBeCalledTimes(1);
      expect(updateRecord).toBeCalledWith({
        ...record,
        condition: "fair",
      });
    });
  });

  test("should update record year correctly", async () => {
    const updateRecord = jest.fn();
    const { getByText, getByLabelText } = renderWithProviders(
      <EditRecord
        onCloseForm={jest.fn()}
        artists={artists}
        record={record}
        updateArtist={jest.fn()}
        updateRecord={updateRecord}
      />
    );

    const updateRecordButton = getByText("Update Record");
    const yearInput = getByLabelText("Year");

    fireEvent.change(yearInput, {
      target: { value: "1999" },
    });

    fireEvent.click(updateRecordButton);

    await waitFor(() => {
      expect(updateRecord).toBeCalledTimes(1);
      expect(updateRecord).toBeCalledWith({
        ...record,
        year: 1999,
      });
    });
  });

  test("should update artist correctly", async () => {
    const updateArtist = jest.fn();
    const { getByText, getByLabelText } = renderWithProviders(
      <EditRecord
        onCloseForm={jest.fn()}
        artists={artists}
        record={record}
        updateArtist={updateArtist}
        updateRecord={jest.fn()}
      />
    );

    const editArtistButton = getByText("Edit Artist");

    fireEvent.click(editArtistButton);

    await waitFor(() => {
      expect(getByText("Update Artist")).toBeInTheDocument();
    });

    const updateArtistButton = getByText("Update Artist");
    const artistInput = getByLabelText("Artist");

    fireEvent.change(artistInput, {
      target: { value: "Test Artist Name New" },
    });

    fireEvent.click(updateArtistButton);

    await waitFor(() => {
      expect(updateArtist).toBeCalledWith({
        id: 1,
        name: "Test Artist Name New",
      });
    });
  });
});
