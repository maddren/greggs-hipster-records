import React, { useEffect, useState, useCallback, FC } from "react";
import { uniqBy } from "lodash";
import styled from "styled-components";

import { IRecord } from "../types/records";
import { recordsEndpoint } from "../constants/records";
import { INextPage, IRecordsResponse } from "../types/records";
import EditRecord from "./EditRecord";
import RecordsList from "./RecordsList";
import { IArtist } from "../types/artists";
import Alert from "../components/Alert";
import { toast } from "react-toastify";

const StyledRecords = styled.div`
  text-align: center;
  margin: 0 auto;
  max-width: 1000px;
`;

const Records: FC = () => {
  const [records, setRecords] = useState<IRecord[]>([]);
  const [nextPage, setNextPage] = useState<INextPage>(null);
  const [isRecordFormActive, setIsRecordFormActive] = useState(false);
  const [selectedRecord, setSelectedRecord] = useState<IRecord>();
  const [dataLoading, setDataLoading] = useState(false);
  const [dataError, setDataError] = useState(false);

  const artists = (r: IRecord[]) =>
    uniqBy(
      r.map((record) => record.artist),
      (artist) => artist.id
    );

  const fetchRecords = useCallback(async (request: RequestInfo) => {
    try {
      setDataLoading(true);
      const res = await fetch(request);
      const data: IRecordsResponse = await res.json();
      const nextRecords = data.results;

      setRecords((prevRecords) => {
        // This is needed if we change an artist and fetch the next page
        const formattedNextRecords = nextRecords.map((record) => {
          const existingArtist = artists(prevRecords).filter(
            (artist) => artist.id === record.artist.id
          )[0];
          return existingArtist
            ? { ...record, artist: existingArtist }
            : record;
        });

        // Place an id against each record
        const recordsWithId = [
          ...prevRecords,
          ...formattedNextRecords,
        ].map((record, i) => ({ ...record, id: i }));

        return recordsWithId;
      });

      setNextPage(data.nextPage);
      setDataLoading(false);
    } catch (err) {
      setDataError(true);
      setDataLoading(false);
    }
  }, []);

  const activateRecordForm = (id: number) => {
    setSelectedRecord(records[id]);
    setIsRecordFormActive(true);
  };

  const updateArtist = (artist: IArtist) => {
    setRecords(
      records.map((record) =>
        record.artist.id === artist.id ? { ...record, artist } : record
      )
    );
    toast.success("Successfully updated artist");
  };

  const updateRecord = (newRecord: IRecord) => {
    setRecords(
      records.map((record, i: number) =>
        i === newRecord.id ? newRecord : record
      )
    );
    toast.success("Successfully updated record");
  };

  useEffect(() => {
    fetchRecords(recordsEndpoint);
  }, [fetchRecords]);

  return (
    <StyledRecords data-testid="records">
      {dataError ? (
        <Alert>An error occured when loading Greg's records</Alert>
      ) : (
        <>
          <RecordsList
            records={records}
            nextPage={nextPage}
            activateRecordForm={activateRecordForm}
            fetchRecords={fetchRecords}
            dataLoading={dataLoading}
          />
          {selectedRecord !== undefined && isRecordFormActive && (
            <EditRecord
              record={selectedRecord}
              artists={artists(records)}
              updateRecord={updateRecord}
              updateArtist={updateArtist}
              onCloseForm={() => setIsRecordFormActive(false)}
            />
          )}
        </>
      )}
    </StyledRecords>
  );
};

export default Records;
