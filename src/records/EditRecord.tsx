import React, { FC, useEffect, useState, ChangeEvent } from "react";
import { useForm, Controller } from "react-hook-form";
import styled from "styled-components";
import { lighten } from "polished";

import { IRecord } from "../types/records";
import { conditions } from "../constants/records";
import { IArtist } from "../types/artists";
import Modal from "../components/Modal";
import Button from "../components/Button";

const StyledEditRecord = styled.div`
  max-width: 600px;
  label {
    font-weight: 600;
    margin-bottom: 0.5rem;
    display: block;
  }
  input,
  select {
    width: 100%;
    margin-bottom: 1rem;
    box-sizing: border-box;
    padding: 1rem 0.5rem;
  }
  .close-button {
    position: absolute;
    right: 0;
    top: 0;
    border: 0;
    background-color: ${({ theme }) => theme.colors.red};
    color: #fff;
    padding: 1rem;
    border-radius: 0 0.25rem 0 0.25rem;
  }
  .form-group {
    padding: 1rem;
    margin-bottom: 1rem;
    background-color: ${({ theme }) => lighten(0.1, theme.colors.navy)};
  }
  .artist-editor {
    background-color: ${({ theme }) => lighten(0.2, theme.colors.navy)};
    padding: 1rem;
    margin-top: 1rem;
  }
  .actions-container {
    display: grid;
    grid-template-columns: 1fr 1fr;
    grid-gap: 1rem;
  }
`;

interface IProps {
  record: IRecord;
  onCloseForm: () => void;
  artists: IArtist[];
  updateArtist: (artist: IArtist) => void;
  updateRecord: (record: IRecord) => void;
}

const EditRecord: FC<IProps> = (props) => {
  const { register, handleSubmit, reset, getValues, control } = useForm();

  const [formArtist, setFormArtist] = useState(props.record.artist);
  const [artistEditorActive, setArtistEditorActive] = useState(false);

  const onSubmit = (formState: IRecord) => {
    // The artist needs to be pulled from the main state as the form state
    // only holds the id
    const artist = props.artists.filter((a) => a.id === formState.artist.id)[0];

    props.updateRecord({
      ...props.record,
      ...formState,
      year: Number(formState.year),
      artist,
    });
  };

  // Handles an artist change on the record
  const handleRecordArtistChange = (
    e: ChangeEvent<HTMLSelectElement>,
    onChange: (id: number) => void
  ) => {
    const id = Number(e.target.value);
    setArtistEditorActive(false);
    setFormArtist(props.artists.filter((a) => a.id === id)[0]);
    onChange(id);
  };

  // Handles a direct change to the artist name across all records
  const handleArtistNameChange = (e: ChangeEvent<HTMLInputElement>) =>
    setFormArtist({
      name: e.target.value,
      id: getValues("artist.id"),
    });

  useEffect(() => {
    reset(props.record);
  }, [props.record, reset]);

  return (
    <Modal isOpen={true}>
      <StyledEditRecord>
        <h2>Edit Record</h2>
        <button
          className="close-button"
          aria-label="Close Modal"
          onClick={props.onCloseForm}
          type="button"
        >
          Close
        </button>
        <form onSubmit={handleSubmit(onSubmit)}>
          <div className="form-item">
            <label htmlFor="album_title">Album Title</label>
            <input
              aria-label="Album Title"
              name="album_title"
              type="text"
              ref={register({ required: true })}
            />
          </div>
          <div className="form-item">
            <label htmlFor="condition">Condition</label>
            <select
              aria-label="Condition"
              name="condition"
              ref={register({ required: true })}
            >
              {conditions.map((condition) => (
                <option key={condition.key} value={condition.key}>
                  {condition.label}
                </option>
              ))}
            </select>
          </div>
          <label htmlFor="artist.id">Artist</label>
          <div className="form-group">
            <Controller
              control={control}
              name="artist.id"
              render={({ onChange, value }) => (
                <select
                  aria-label="Record Artist"
                  value={value}
                  onChange={(e) => handleRecordArtistChange(e, onChange)}
                >
                  {props.artists.map((artist) => (
                    <option value={artist.id} key={`artist-${artist.id}`}>
                      {artist.name}
                    </option>
                  ))}
                </select>
              )}
            />
            <Button
              type="button"
              onClick={() => setArtistEditorActive(!artistEditorActive)}
            >
              {artistEditorActive ? "Close Artist Editor" : "Edit Artist"}
            </Button>
            {artistEditorActive && (
              <div className="artist-editor">
                <p>
                  If the artist is updated it will change across all of your
                  records.
                </p>
                <input
                  aria-label="Artist"
                  type="text"
                  value={formArtist.name}
                  onChange={handleArtistNameChange}
                />
                <Button
                  onClick={() => props.updateArtist(formArtist)}
                  aria-label="Update Artist"
                  variant="primary"
                  type="button"
                >
                  Update Artist
                </Button>
              </div>
            )}
          </div>
          <div className="form-item">
            <label htmlFor="year">Year</label>
            <input
              aria-label="Year"
              name="year"
              type="number"
              ref={register({ required: true })}
            />
          </div>

          <div className="actions-container">
            <Button variant="primary" aria-label="Update Record">
              Update Record
            </Button>
            <Button
              type="button"
              variant="secondary"
              aria-label="Close Modal"
              onClick={props.onCloseForm}
            >
              Close
            </Button>
          </div>
        </form>
      </StyledEditRecord>
    </Modal>
  );
};

export default EditRecord;
