import React, { FC } from "react";
import Header from "./Header";
import Footer from "./Footer";

const Layout: FC = ({ children }) => (
  <>
    <Header />
    <main>{children}</main>
    <Footer />
  </>
);

export default Layout;
