import React, { FC } from "react";
import styled from "styled-components";

const StyledFooter = styled.footer`
  text-align: center;
  padding: 2rem 0;
  a {
    color: #fff;
  }
`;

const Footer: FC = () => (
  <StyledFooter data-testid="footer">
    <p>&copy; Greg's Hipster Records</p>
    <div>
      Icons made by{" "}
      <a
        href="https://www.flaticon.com/authors/monkik"
        title="monkik"
        target="_blank"
        rel="noopener noreferrer"
      >
        monkik
      </a>
    </div>
  </StyledFooter>
);

export default Footer;
