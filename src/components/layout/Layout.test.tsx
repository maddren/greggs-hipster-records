import React from "react";
import { render } from "@testing-library/react";
import Layout from "./Layout";

describe("Layout", () => {
  test("renders layout with header, child and footer", () => {
    const { getByText, getByTestId } = render(
      <Layout>
        <p>layout children</p>
      </Layout>
    );
    expect(getByTestId("header")).toBeInTheDocument();
    expect(getByText("layout children")).toBeInTheDocument();
    expect(getByTestId("footer")).toBeInTheDocument();
  });
});
