import React, { FC } from "react";
import styled from "styled-components";
import hipster from "../../assets/hipster.png";

const StyledHeader = styled.header`
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 2rem;
  font-weight: 700;
  padding: 2rem 0;
  img {
    max-width: 3.5rem;
    margin-right: 1rem;
  }
`;

const Header: FC = () => (
  <StyledHeader data-testid="header">
    <img src={hipster} alt="Hipster" />
    Greg's Hipster Records
  </StyledHeader>
);

export default Header;
