import React from "react";
import { render } from "@testing-library/react";
import Footer from "./Footer";

describe("Footer", () => {
  test("renders footer", () => {
    const { getByText } = render(<Footer />);

    expect(getByText("© Greg's Hipster Records")).toBeInTheDocument();
  });
});
