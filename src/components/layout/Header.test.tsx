import React from "react";
import { render } from "@testing-library/react";
import Header from "./Header";

describe("Header", () => {
  test("renders header", () => {
    const { getByText } = render(<Header />);

    expect(getByText("Greg's Hipster Records")).toBeInTheDocument();
  });
});
