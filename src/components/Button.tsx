import styled from "styled-components";
import React, { FC } from "react";

const StyledButton = styled.button<ICommonProps>`
  ${({ variant, theme }) =>
    variant === "primary"
      ? `
        background-color: ${theme.colors.blue};
        color: #fff;
    `
      : `
        background-color: ${theme.colors.grey};
        color: ${theme.colors.navy};
    `};
  padding: 0.5rem 1rem;
  border: 0;
  border-radius: 0.25rem;
`;

type IVariant = "primary" | "secondary";

type IType = "submit" | "button";

interface ICommonProps {
  variant?: IVariant;
}

interface IProps extends ICommonProps {
  className?: string;
  ariaLabel?: string;
  type?: IType;
  onClick?: () => void;
  disabled?: boolean;
}

const Button: FC<IProps> = ({
  children,
  onClick,
  variant,
  type,
  ariaLabel,
  className,
  disabled,
}) => (
  <StyledButton
    className={className}
    onClick={onClick}
    type={type}
    variant={variant}
    aria-label={ariaLabel}
    disabled={disabled}
  >
    {children}
  </StyledButton>
);

export default Button;
