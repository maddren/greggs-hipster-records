import React, { FC } from "react";
import styled from "styled-components";
import ReactModal from "react-modal";
import { lighten } from "polished";

interface IProps extends ReactModal.Props {
  className?: string;
}

const ReactModalAdapter: FC<IProps> = ({ className, ...props }: IProps) => {
  const contentClassName = `${className}__content`;
  const overlayClassName = `${className}__overlay`;
  return (
    <ReactModal
      portalClassName={className}
      className={contentClassName}
      overlayClassName={overlayClassName}
      {...props}
    />
  );
};

export const Modal = styled(ReactModalAdapter)`
  &__overlay {
    position: fixed;
    top: 0px;
    left: 0px;
    right: 0px;
    bottom: 0px;
    background-color: rgba(0, 0, 0, 0.75);
  }

  &__content {
    position: absolute;
    top: 40px;
    left: 40px;
    right: 40px;
    bottom: 40px;
    border: 1px solid ${({ theme }) => lighten(0.1, theme.colors.navy)};
    background: ${({ theme }) => theme.colors.navy};
    overflow: auto;
    border-radius: 4px;
    outline: none;
    padding: 20px;
  }
`;

export default Modal;
