import React, { FC } from "react";
import styled from "styled-components";

const StyledAlert = styled.div<IProps>`
  padding: 1rem;
  background-color: ${({ theme, variant }) =>
    variant === "success" ? theme.colors.green : theme.colors.red};
  border-radius: 0.5rem;
  font-weight: 600;
`;

type IVariant = "warning" | "success";

interface IProps {
  variant?: IVariant;
}

const Alert: FC<IProps> = ({ children, variant }) => (
  <StyledAlert variant={variant}>{children}</StyledAlert>
);

export default Alert;
