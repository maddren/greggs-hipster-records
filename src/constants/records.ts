import { ICondition } from "../types/records";

export const recordsEndpoint =
  "https://gist.githubusercontent.com/seanders/df38a92ffc4e8c56962e51b6e96e188f/raw/b032669142b7b57ede3496dffee5b7c16b8071e1/page1.json";

export const conditions: { key: ICondition; label: string }[] = [
  { key: "poor", label: "Poor" },
  { key: "fair", label: "Fair" },
  { key: "good", label: "Good" },
  { key: "very_good", label: "Very Good" },
  { key: "mint", label: "Mint" },
];
