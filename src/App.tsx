import React, { useEffect, FC } from "react";
import Modal from "react-modal";
import { createGlobalStyle, ThemeProvider } from "styled-components";
import WebFont from "webfontloader";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
// @ts-ignore
import normalize from "normalize.css";

import { Layout } from "./components/layout";
import Records from "./records/Records";
import theme from "./theme";

const GlobalStyle = createGlobalStyle`
  ${normalize};

  body {
    font-family: "Lato", sans-serif;
    background-image: url(https://images.unsplash.com/photo-1522039176993-33264c54767e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2208&q=80&bri=-30);
    background-repeat: no-repeat;
    background-size: cover;
    color: #fff;
  }
`;

const App: FC = () => {
  useEffect(() => {
    Modal.setAppElement("#root");
    WebFont.load({
      google: {
        families: ["Lato"],
      },
    });
  }, []);
  return (
    <div className="App">
      <ThemeProvider theme={theme}>
        <GlobalStyle />
        <Layout>
          <Records />
        </Layout>
        <ToastContainer position="bottom-right" />
      </ThemeProvider>
    </div>
  );
};

export default App;
