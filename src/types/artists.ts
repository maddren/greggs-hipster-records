export interface IArtist {
  name: string;
  id: number;
}
