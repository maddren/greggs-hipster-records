import { IArtist } from "./artists";

export type ICondition = "poor" | "very_good" | "fair" | "mint" | "good";

export interface IRecord {
  id: number;
  album_title: string;
  year: number;
  condition: ICondition;
  artist: IArtist;
}

export type INextPage = RequestInfo | null;
export type IRecordsResponse = { results: IRecord[]; nextPage: INextPage };
