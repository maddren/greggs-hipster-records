const theme = {
  colors: {
    navy: "#243045",
    blue: "#5067EB",
    grey: "#F0F4FF",
    green: "#60CA7C",
    red: "#F56565",
  },
};

export default theme;
